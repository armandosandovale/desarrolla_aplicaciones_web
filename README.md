"Crea aplicaciones WEB con conexión a base de datos"
"5AVP"
"Sandoval Espinoza Armando"

Practica #1 - 02/09/2022 - Practica de ejemplo
Commit: 4df6960c0fea4ff7725fc229920912dc4c91f3ec
Archivo: https://gitlab.com/armandosandovale/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html

Practica #2 - 09/09/22 - Practica Mensajes JavaScript
Commit: a9a526d72526bf1a84619983c3cac76bece4bc66
Archivo: https://gitlab.com/armandosandovale/desarrolla_aplicaciones_web/-/blob/main/parcial1/Practica_JavaScript.html

Practica #3 - 15/09/22 - Menu de navegación y pantalla
Commit: 620d6d9c8827e52246d40828628f83523c05d476
Archivo: https://gitlab.com/armandosandovale/desarrolla_aplicaciones_web/-/blob/main/parcial1/desarrolla_aplicaciones_web-main.zip

Practica #4 - 19/09/2022 -  Practica web con base de datos - Visita de consulta de datos
Commit:  4cc73f3e268e159246b97e2a02bd82a83ed7caca
Archivo: https://gitlab.com/armandosandovale/desarrolla_aplicaciones_web/-/tree/main/parcial1/practicaWEBDatos

Practica #5 - 22/09/2022 -  Vista de Registro de datos
Commit:  accc3b731e3e54ef0a63b8fff675a20d308fb59f
Archivo: https://gitlab.com/armandosandovale/desarrolla_aplicaciones_web/-/blob/main/parcial1/practicaWEBDatos/registrarDatos.html

Practica #6 - 26/09/2022 -  Mostrar Registros en consulta de datos
consultarDatos:
Commit:  803ea3763fbafb0c1a2672032f3855bfd723d722
Archivo: https://gitlab.com/armandosandovale/desarrolla_aplicaciones_web/-/blob/main/parcial1/practicaWEBDatos/consultarDatos.php

conexionPHP:
Commit: 59e98588813b83a227b5936196370d02d78df5a3
Archivo: https://gitlab.com/armandosandovale/desarrolla_aplicaciones_web/-/blob/main/parcial1/practicaWEBDatos/conexionPHP.php

